![banner](css/portada-web.png)

# Titulo del Proyecto
#### Curriculum Vitae

## índice
1. [Caracteristicas del proyecto](#caracteristicas)
2. [Contenido del Proyecto](#contenido-del-proyecto)
3. [Tecnologías](#Tecnologías)
4. [IDE´S](#ides)
5. [Instalación](#instalación)
6. [Versión Demo](#versión-demo)
7. [Autor(es)](#autores)
8. [Institución Academica](#institución-academica)


#### Caracteristicas
- Uso de Css recomendado: [Ver](https://gitlab.com/JohnnyQuintero/curriculum/-/tree/master/css)

#### Contenido del proyecto
- [index.html](https://gitlab.com/JohnnyQuintero/curriculum/-/blob/master/index.html): Visualización de la pagina preincipal

#### Tecnologías
- Uso de Boostrap: [ir](https://getboostrap.com)
- Aprende Boostrap: [ver](https:youtube.com/watch?v=Zuasdfl_ASadf)
#### IDE´S
- Sublime text
#### Instalación
    1. local
     - Descarga el repositorio ubicado en [descargar](https://gitlab.com/JohnnyQuintero/curriculum/)
     - Abrir el archivo index.html desde el navegador preterminado
    2. gitlab
     - Realizando un fork
#### Versión Demo
Se puede visualizar la veesión demo [ver](https://johnnyquintero.gitlab.io/curriculum/)
#### Autor(es)
Realizado por: [Johnny Alexander Quintero Reyes](<johnnyalexanderqr@ufps.edu.co>)
#### Instalación Academica
Proyecto desarrollado en la materia de programación Web de la [UFPS](https://ww2.ufps.edu.co)
